import {
  Directive,
  OnInit,
  HostListener,
  Renderer2,
  ElementRef,
} from '@angular/core';

@Directive({
  selector: '[appClickDisappear]',
})
export class ClickDisappearDirective implements OnInit {
  constructor(private renderer: Renderer2, private element: ElementRef) {}

  @HostListener('click', ['$event'])
  vanish(ev) {
    console.log('vanish triggered');
    this.renderer.setStyle(this.element.nativeElement, 'opacity', '0');
  }

  ngOnInit() {
    this.renderer.setStyle(
      this.element.nativeElement,
      'transition',
      'opacity 2s linear'
    );
  }
}
